'use strict';

var aurigaServiceUrl = 'http://localhost:8080/EDDB/service/';

/* App Module */
var app = angular.module('AurigaBase', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider',	function($routeProvider, $locationProvider) {
	$routeProvider.when('/', {
		templateUrl: 'navigation.html',
		controller: 'NavigationCtrl'
	})
	.when('/navigation/:systemName', {
		templateUrl: 'navigation.html',
		controller: 'NavigationCtrl',
	})
	.when('/systemSearch', {
		templateUrl: 'systemSearch.html',
		controller: 'SystemSearchCtrl'
	})
	.when('/login', {
		templateUrl: 'login.html',
		controller: 'LoginCtrl'
	})
}]);

app.run(function($rootScope, $location) {
	$rootScope.$on("$routeChangeStart", function(event, next, current) {
		if($rootScope.loggedUser == null) {
			console.log(next);
		}
	})
})
