'use strict';

/* Services */

var app = angular.module('AurigaBase');

app.factory('globals', function() {
	return {
		isSearchResult: false,
		currentSystem : {
			name : 'Sol (Home of Mankind)'
		}
	}
})