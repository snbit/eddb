'use strict';

/* Controllers */

var aurigaServiceUrl = 'http://localhost:8080/EDDB/service/';

var app = angular.module('AurigaBase');

app.controller('NavigationCtrl', function($rootScope, $scope, $http, globals) {
	$scope.currentSystem = globals.currentSystem;
	$scope.isSearchResult = globals.isSearchResult;
	$scope.searchResult = {};

	$rootScope.$watch('isSearchResult', function(newVal, oldVal) {
		if($scope.currentSystem.name != null && $scope.currentSystem.name != '') {
			$scope.search($scope.currentSystem.name, 10)
		}
	})
	

	
	$scope.search = function(name, radius) {
		// $http.get('http://localhost:8080/EDDB/service/getNeighbours?name='+name+'&radius='+radius).
		$http.get(
				aurigaServiceUrl + 'getNeighbours?name=' + name + '&radius='
						+ radius).success(
				function(data, status, headers, config) {

					if (data != null) {
						globals.isSearchResult = true;
					}
					$scope.searchResult = data;
					for (var i = 0; i < $scope.searchResult.length; i++) {
						$scope.searchResult[i].distance = $scope
								.calculateDistance($scope.searchResult[i]);
					}
				}).error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		});
	};
	

	  
	$scope.calculateDistance = function(system) {
		var sel = $scope.currentSystem;
		var dist = Math
				.sqrt(Math.pow(system.x - sel.x, 2)
						+ Math.pow(system.y - sel.y, 2)
						+ Math.pow(system.z - sel.z, 2)) / 32;
		return dist;
	}
})

app.controller('LoginCtrl', function($scope) {

})

app.controller('SystemSearchCtrl', function($scope, $http, globals) {
	$scope.items = [];
	$scope.currentSystem = globals.currentSystem;
	$scope.systemName = '';
	$scope.searchResult = {};

	
	$scope.selected = function(system) {
		//$scope.currentSystem = system;
		globals.currentSystem = system;
		history.back();
	}

	$scope.typed = function() {
		if ($scope.systemName.length > 1) {
			$http.get(aurigaServiceUrl + 'system?search=' + $scope.systemName)
					.success(function(data, status, headers, config) {

						if (data != null) {
							globals.isSearchResult = true;
						}
						$scope.searchResult = data;
					}).error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
		}
	};

	$scope.cancel = function() {
		history.back();
	}
})
