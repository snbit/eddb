'use strict';

var aurigaServiceUrl = 'http://localhost:8080/EDDB/service/';

/* App Module */
var app = angular.module('AurigaBase', ['ui.bootstrap']);

app.controller('SystemSearchCtrl', function($scope, $http, $modal, $log) {
  $scope.isSearchResult = false;
  $scope.searchResult = {};
  $scope.search = function(name, radius) {
	  //$http.get('http://localhost:8080/EDDB/service/getNeighbours?name='+name+'&radius='+radius).
	  $http.get(aurigaServiceUrl +'getNeighbours?name='+name+'&radius='+radius).
	  success(function(data, status, headers, config) {
		  
		  if(data != null){
			  $scope.isSearchResult = true;
		  }
		  $scope.searchResult = data;
		  for(var i = 0; i< $scope.searchResult.length; i++) {
			  $scope.searchResult[i].distance = $scope.calculateDistance($scope.searchResult[i]);
		  }
	  }).
	  error(function(data, status, headers, config) {
	    // called asynchronously if an error occurs
	    // or server returns response with an error status.
	  });
  };
  
  $scope.radius = 10;
  $scope.radiusSub = function() {
	  $scope.radius--;
  }
  $scope.radiusAdd = function() {
	  $scope.radius++;
  }
  
  $scope.calculateDistance = function(system) {
	  var sel = $scope.selected;
	  var dist = Math.sqrt(Math.pow(system.x-sel.x, 2) + Math.pow(system.y-sel.y, 2) + Math.pow(system.z-sel.z, 2))/32;
	  return dist;
  }
  

	  $scope.items = [];
	  $scope.selected = {
		name: 'Choose System'
	  };
	  $scope.open = function (size) {

	    var modalInstance = $modal.open({
	      templateUrl: 'components/searchSystemDialog.html',
	      controller: 'ModalInstanceCtrl',
	      size: size,
	      resolve: {
	        items: function () {
	          return $scope.items;
	        }
	      }
	    });

	    modalInstance.result.then(function (selectedItem) {
	      $scope.selected = selectedItem;
	      $scope.search($scope.selected.name, $scope.radius);
	    }, function () {
	      $log.info('Modal dismissed at: ' + new Date());
	    });
	  };
	});

	// Please note that $modalInstance represents a modal window (instance) dependency.
	// It is not the same as the $modal service used above.

app.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items, $http) {
	$scope.items = items;
	$scope.selected = {
		item : $scope.items[0]
	};
	$scope.searchResult = {};
	$scope.systemName = '';
	$scope.typed = function() {
		if ($scope.systemName.length > 4) {
			$http.get(
					aurigaServiceUrl +'system?search='
							+ $scope.systemName).success(
					function(data, status, headers, config) {

						if (data != null) {
							$scope.isSearchResult = true;
						}
						$scope.searchResult = data;
					}).error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
		}
	};

	$scope.ok = function() {
		$modalInstance.close($scope.selected.item);
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});


