<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<script type="text/javascript">
	//default values
	dVersion = 2;
	dTest = true;
	dOutputMode = 2;
	dKnownStatus = 0; // 0=all, 1=known coordinates, 2=unknown coordinates
	dSystemName = "Sol";
	dCr = 5;
	dCreationDate = "2014-12-18 12:34:56";
	dCoordCube = null;
	dRadius = null;
	dX = null;
	dY = null;
	dZ = null;
	eVersion = document.getElementById("version");
	eTest = document.getElementById("test");
	eOutputMode = document.getElementById("outputMode");
	eKnownStatus = document.getElementById("status");
	eSystemName = document.getElementById("name");
	eCr = document.getElementById("rating");
	eCreationDate = document.getElementById("date");
	eCoordCube = document.getElementById("cube");
	eRadius = document.getElementById("radius");
	eX = document.getElementById("originx");
	eY = document.getElementById("originy");
	eZ = document.getElementById("originz");
	function doPreset(version, test, outputMode, knownStatus, systemName, cr, creationDate, coordCube, radius, x, y, z) {
		if(version == null) {
			eVersion.disabled = true;
		} else {
			document.getElementById("version").value = version;
		}
	}
	function doPresetDefault(){
		doPreset();
	}
	function enableAll(){
		eVersion.disabled = true;
	}
</script>

<body>
<p>Presets:</p>
<ul>
	<li><a href="javascript:doPresetDefault()">Reset form to default</a></li>
	<li><a href="javascript:enableAll()">Enable all fields</a></li>
</ul>
<form action="test" method="get">
  <table border="0" cellpadding="5" cellspacing="0" bgcolor="#E0E0E0">
    <tr>
      <td align="right">Version:</td>
      <td><input name="version" id="version" type="number" size="1" maxlength="1" value = "2"></td>
    </tr>
    <tr>
      <td align="right">Test:</td>
      <td><input type="checkbox" name="test" id="test" type="number" size="10" maxlength="10" checked></td>
    </tr>
    <tr>
      <td align="right">Outputmode:</td>
      <td>
      <select name="outputmode" id="outputmode" size="1">
      	<option>1</option>
      	<option>2</option>
      </select>
      </td>
    </tr>
    <tr>
      <td align="right">Known Status:</td>
      <td><input name="status" id="status" type="number" size="1" maxlength="1" value = "0"></td>
    </tr>
    <tr>
      <td align="right">System Name:</td>
      <td><input name="name" id="name" type="text" size="20" maxlength="30" value = "Sol"></td>
    </tr>
    <tr>
      <td align="right">Confidence Rating:</td>
      <td><input name="rating" id="rating" type="number" size="1" maxlength="1" value = "5"></td>
    </tr>
    <tr>
      <td align="right">Creation Date:</td>
      <td><input name="date" id="date" type="text" size="19" maxlength="19" value = "2014-09-18 12:34:56"></td>
    </tr>
    <tr>
      <td align="right">Coord Cube:</td>
      <td><input name="cube" id="cube" type="text" size="30" maxlength="45" value = "[[-10,10],[-10,10],[-10,10]]" disabled="disabled"></td>
    </tr>
    <tr>
      <td align="right">Coord Sphere Radius:</td>
      <td><input name="radius" id="radius" type="number" size="5" maxlength="10" value = "12,45"></td>
    </tr>
    <tr>
      <td align="right">Coord Sphere Origin x:</td>
      <td><input name="originx" id="originx" type="number" size="1" maxlength="1" value = "0"></td>
    </tr>
    <tr>
      <td align="right">Coord Sphere Origin y:</td>
      <td><input name="originy" id="originy" type="number" size="1" maxlength="1" value = "0"></td>
    </tr>
    <tr>
      <td align="right">Coord Sphere Origin z:</td>
      <td><input name="originz" id="originz" type="number" size="1" maxlength="1" value = "0"></td>
    </tr>
    <tr>
      <td align="right"></td>
      <td>
        <input type="submit" value="Submit">
      </td>
    </tr>
  </table>
</form>
</body>
</html>