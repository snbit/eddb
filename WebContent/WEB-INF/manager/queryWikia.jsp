<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="queryWikia" class="de.sntr.eddb.crawler.EliteDangerousWikiaCrawler" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EliteDangerousWikiaCrawler Test</title>
</head>
<body style="font-family:Arial">
<c:forEach items="${queryWikia.getSystemsAsJsonStrings()}" var="item">
	<p>${item}</p>
</c:forEach>
</body>
</html>