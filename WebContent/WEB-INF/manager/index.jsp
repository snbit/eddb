<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
if(session.isNew() || request.getParameter("password") == null || !request.getParameter("password").equals("magic")) {
	getServletContext().getRequestDispatcher("/login").forward(request, response);
}
%>
<html>
<link rel="stylesheet" type="text/css" href="/EDDB/css/manager.css">
<!--style type="text/css">
.left {
	text-align:left;
}
</style-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDDB Manager</title>
</head>
<body style="width:70%; margin:0 auto; font-family:Arial">
<h1>EDDB Manager</h1>

<h2>Tests that don't change the database</h2>
<table border=1 cellpadding="15">
<colgroup>
	<col width="1%" align="left">
	<col width="98%">
	<col width="1%" style="text-align:center">
</colgroup>
<tr>
	<th class="left">Test</th>
	<th>Description</th>
	<th>Action</th>
</tr>
<tr>
	<td class="top"></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td class="top">Query EDSC</td>
	<td>Test the EDSC Crawler.</td>
	<td><input type="button" onclick="location.href='/EDDB/manager/queryEDSC'" value="Ring bell"></td>
</tr>
<tr>
	<td class="top">Query Wikia</td>
	<td>Test the Wikia Crawler</td>
	<td><input type="button" onclick="location.href='/EDDB/manager/queryWikia'" value="Say hello"></td>
</tr>
<tr>
	<td class="top"></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td class="top"></td>
	<td></td>
	<td></td>
</tr>
</table>

<h2>Tools that DO change the database</h2>
<table border=1 cellpadding="15">
<colgroup>
	<col width="1%" align="left">
	<col width="98%">
	<col width="1%" style="text-align:center">
</colgroup>
<tr>
	<th class="left">Tool</th>
	<th>Description</th>
	<th>Action</th>
</tr>
<tr>
	<td class="top"></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td class="top">GenerateCommonData</td>
	<td>Will update tables like *Categories, StarClass and stuff, StationTypes.*</td>
	<td><input type="button" onclick="location.href='/EDDB/generateCommonData'" value="Click Me"></td>
</tr>
<tr>
	<td class="top">CrawlEDSC</td>
	<td>Will update systems in the database with EDSC's GetSystems data. Empty database (=earliest update date) will need a complete download which will take a minute.
	Otherwise only the latest update date found in the database will be requested.</td>
	<td><input type="button" onclick="location.href='/EDDB/manager/crawlEdsc'" value="I want it"></td>
</tr>
<tr>
	<td class="top">CrawlWikia</td>
	<td>Currently updating Systems.</td>
	<td><input type="button" onclick="location.href='/EDDB/manager/crawlWikia'" value="Do it"></td>
</tr>
<tr>
	<td class="top"></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td class="top"></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td class="top">DropCreateEDDB</td>
	<td>Drop all tables and create new. All data will be gone.</td>
	<td><input type="button" onclick="location.href='/EDDB/css/manager.css'" value="Cool"></td>
</tr>
</table>

</body>
</html>