package de.sntr.eddb.crawler;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public abstract class Crawler implements Runnable {
	
	protected String remoteAddress = null;
	protected CloseableHttpClient httpClient = null;
	
	protected Crawler(String remoteAddress) {
		httpClient = HttpClients.createDefault();
		this.remoteAddress = remoteAddress;
	}
	
}
