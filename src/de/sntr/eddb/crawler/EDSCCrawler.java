package de.sntr.eddb.crawler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import de.sntr.eddb.crawler.edsc.EDSCFilter;
import de.sntr.eddb.crawler.edsc.EDSCGetSystemsRequest;
import de.sntr.eddb.crawler.edsc.EDSCGetSystemsResponse;
import de.sntr.eddb.crawler.edsc.EDSCSystem;


public class EDSCCrawler extends Crawler {
	
	public static Date oldestDate;
	public static SimpleDateFormat edscFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final Logger log = Logger.getLogger(this.getClass().getName());
	
	public EDSCCrawler() {
		super("http://www.edstarcoordinator.com/api.asmx/");
		try {
			oldestDate = DateUtils.parseDate("191014", "ddMMyy");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		Date currentDate = new Date(oldestDate.getTime());
		try {
			currentDate = DateUtils.parseDate("281114", "ddMMyy");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Date now = new Date();
		
		EDSCGetSystemsRequest data = new EDSCGetSystemsRequest();
		data.setFilter(new EDSCFilter());
		Gson gson = new Gson();
        HttpPost post = new HttpPost(remoteAddress +"GetSystems");
		CloseableHttpResponse response = null;
        
		data.getFilter().setDate(edscFormat.format(currentDate));

		currentDate = DateUtils.addWeeks(currentDate, 1);

		StringEntity params = null;
		try {
			String json = "{\"data\":" + gson.toJson(data) + "}";
			// System.out.println(json);
			params = new StringEntity(json);
			params.setContentType("application/json");
			params.setContentEncoding("UTF-8");
			post.setEntity(params);
			response = httpClient.execute(post);
			String result = EntityUtils.toString(response.getEntity(), "UTF-8");
			result = result.substring(5, result.length() - 1);
			EDSCGetSystemsResponse edscResponse = gson.fromJson(result,
					EDSCGetSystemsResponse.class);
			System.out.println(edscResponse.toString());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Set<de.sntr.eddb.data.System> crawlStarSystems(Date updateDate) {
		Set<de.sntr.eddb.data.System> systems = new HashSet<de.sntr.eddb.data.System>();
		
		EDSCGetSystemsRequest data = new EDSCGetSystemsRequest();
		EDSCFilter filter = new EDSCFilter();
		filter.setCr(5);
		filter.setDate(edscFormat.format(updateDate));
		filter.setKnownstatus(1);
		data.setFilter(filter);
		data.setTest(true);
		data.setVer(2);
		data.setOutputmode(2);
		
		Gson gson = new Gson();
		String json = "{\"data\":" + gson.toJson(data) + "}";
		
		HttpPost post = new HttpPost(remoteAddress +"GetSystems");
		CloseableHttpResponse response = null;
		
		StringEntity params = null;
		String result = null;
		// System.out.println(json);
		try {
			params = new StringEntity(json);
			params.setContentType("application/json");
			params.setContentEncoding("UTF-8");
			post.setEntity(params);
			response = httpClient.execute(post);
			result = EntityUtils.toString(response.getEntity(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SocketException e) {
			log.severe("SocketException: Connection reset");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result == null) {
			
			return null;
		}
		result = result.substring(5, result.length() - 1);
		EDSCGetSystemsResponse edscResponse = gson.fromJson(result, EDSCGetSystemsResponse.class);
		for (EDSCSystem system : edscResponse.getSystems()) {
			de.sntr.eddb.data.System sys = new de.sntr.eddb.data.System(system.getName());
			sys.setX((int)(system.getCoord()[0] *32));
			sys.setY((int)(system.getCoord()[1] *32));
			sys.setZ((int)(system.getCoord()[2] *32));
			systems.add(sys);
		}
//		System.out.println(edscResponse.toString());
		return systems;
	}

		
	
	public static void main(String[] args) {
		new EDSCCrawler().run();
	}
}
