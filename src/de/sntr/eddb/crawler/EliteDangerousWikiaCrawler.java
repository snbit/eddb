package de.sntr.eddb.crawler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;

import de.sntr.eddb.data.Allegiance;
import de.sntr.eddb.data.Faction;
import de.sntr.eddb.data.Government;
import de.sntr.eddb.data.Security;
import de.sntr.eddb.data.Ship;
import de.sntr.eddb.data.State;
import de.sntr.eddb.utils.HttpRequestUtil;
import de.sntr.eddb.utils.UrlUtil;

public class EliteDangerousWikiaCrawler extends Crawler {

	private final String commodities = "/wiki/Commodities";
	private final String ships = "/wiki/Ships";
	private final String blackMarketPricesAndLocations = "/wiki/Black_Market";
	private final String minorFactions = "/wiki/List_of_minor_factions";
	private final String starSystems = "/wiki/Star_Systems";
	
	private final Logger log = Logger.getLogger(EliteDangerousWikiaCrawler.class.getName());
	
	public EliteDangerousWikiaCrawler() {
		super("http://elite-dangerous.wikia.com");
	}
	
	@Override
	public void run() {
		HttpGet httpGet = new HttpGet(remoteAddress +"/wiki/Ships");
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(httpGet);
			List<Ship> result = crawlShipData(IOUtils.toString(response.getEntity().getContent()));
			for(Ship ship:result) {
				System.out.println(ship.getName() +" " +ship.getPrice() +" " +ship.getCargoCapacity() +" " +ship.getCargoMaxCapacity() +" " +ship.getJumpRange());
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		    try {
				response.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public Set<de.sntr.eddb.data.System> crawlStarSystems() {
		
		String response = HttpRequestUtil.request(remoteAddress +starSystems); 
		Set<de.sntr.eddb.data.System> systems = new HashSet<>();
		log.info("Crawling system names");
		Document doc = Jsoup.parse(response);
		Element mwPages = doc.getElementById("mw-pages");
		Element tableTr = mwPages.child(2).child(0).child(0);
		List<Element> as = tableTr.getElementsByTag("a");
		for (Element a : as) {
			if(a.text().contains("Star System Template")) continue;
			de.sntr.eddb.data.System system = new de.sntr.eddb.data.System(a.text());
			systems.add(system);
		}
//Gson gson = new Gson();		
		Map<Faction, Faction> factions = new HashMap<>();
		Map<Government, Government> governments = new HashMap<>();
		Map<Allegiance, Allegiance> allegiances = new HashMap<>();
		Map<Security, Security> securities = new HashMap<>();
		Map<State, State> states = new HashMap<>();
//		int count = 5;
		for (de.sntr.eddb.data.System system : systems) {
			log.info("Crawling data for " +system.getName());
			String respons = HttpRequestUtil.request(UrlUtil.urlEncode("elite-dangerous.wikia.com", "/wiki/" +system.getName()));
			doc = Jsoup.parse(respons);
			Element mwContent = doc.getElementById("mw-content-text");
			List<Element> ths = mwContent.getElementsByTag("th");
			boolean systemNameMatch = true;
			for (Element th : ths) {
				if(!systemNameMatch) {
					break;
				}
				Element td = th.nextElementSibling();
				
				if(td.text().contains("{{{")) {
					continue;
				} else if (td.text().isEmpty()){
					continue;
				} else if (td.text().contains("---")) {
					continue;
				}
				
				switch (th.text()) {
				case "System Name":
					if (!td.text().equals(system.getName())) {
						log.warning("System name does not match: " +system.getName() +", found: " +td.text());
						system.setName(null);
						systemNameMatch = false;
						continue;
					}
					break;
				case "Controlling Faction":
					Faction faction = new Faction();
					faction.setName(td.text());
					if(!factions.containsKey(faction)){
						factions.put(faction, faction);
					} else {
						faction = factions.get(faction);
					}
					system.setFaction(faction);
					break;
				case "Government":
					Government gov = new Government();
					gov.setName(td.text());
					if(!governments.containsKey(gov)){
						governments.put(gov, gov);
					} else {
						 gov = governments.get(gov);
					}
					system.setGovernment(gov);
					break;
				case "State":
					State state = new State();
					state.setName(td.text());
					if(!states.containsKey(state)){
						states.put(state, state);
					} else {
						state = states.get(state);
					}
					system.setState(state);
					break;
				case "Allegiance":
					Allegiance all = new Allegiance();
					all.setName(td.text());
					if(!allegiances.containsKey(all)){
						allegiances.put(all, all);
					} else {
						 all = allegiances.get(all);
					}
					system.setAllegiance(all);
					break;
				case "Population":
					long pop = 0;
					String parse = td.text();
					try {
						if(parse.contains(" Million")) {
							parse = parse.replace(" Million", "");
							float f = Float.parseFloat(parse);
							pop = new Float(f* 1000000).longValue();
						} else if(parse.contains(" Billion")) {
							parse = parse.replace(" Billion", "");
							float f = Float.parseFloat(parse);
							pop = new Float(f* 1000000).longValue() *1000000;
						} else {
							pop = Long.parseLong(td.text().replace(",", ""));
						}
					} catch (NumberFormatException e) {
						log.warning("Could not parse population for value: " +parse);
					}
					
					system.setPopulation(pop);
					break;
				case "Security":
					Security sec = new Security();
					sec.setName(td.text());
					if(!securities.containsKey(sec)){
						securities.put(sec, sec);
					} else {
						 sec = securities.get(sec);
					}
					system.setSecurity(sec);
					break;
				default:
					log.warning("Unknown <th> element: " +th.text());
					break;
				}
			}
//			if(--count==0) {
//				break;
//			}
//System.out.println(gson.toJson(system));
		}

		return systems;
		
	}
	
	private List<Ship> crawlShipData(String response) {
		List<Ship> result = new ArrayList<>();
		String sub = response.substring(response.indexOf("Current Playable Ships"), response.length());
		sub = sub.substring(sub.indexOf("<table"), sub.indexOf("</table>"));
		//Document doc = builder.parse(IOUtils.toInputStream(sub));
		Document doc = Jsoup.parse(sub);
		Elements elements = doc.getElementsByTag("tr");
		for (int i = 1; i < elements.size(); i++) {
			Ship ship = new Ship();
			Element e = elements.get(i);
			ship.setName(e.child(0).text());
			ship.setPrice(Integer.parseInt(e.child(2).text().substring(0, e.child(2).text().length()-1).replace(",", "")));
			ship.setCargoCapacity(Short.parseShort(e.child(3).text()));
			ship.setCargoMaxCapacity(Short.parseShort(e.child(4).text()));
			String text = e.child(5).text();
			if(text.contains("fully loaded")) {
				text = text.replace("fully loaded", "");
			}
			else if (text.isEmpty()) {
				text = "0";
			}
			ship.setJumpRange(Float.parseFloat(text));
			result.add(ship);
			//...more to crawl
		}
		return result;
	}
	
	public List<String> getSystemsAsJsonStrings() {
		List<String> result = new ArrayList<>();
		Gson gson = new Gson();
		
		Map<Faction, Faction> factions = new HashMap<>();
		Map<Government, Government> governments = new HashMap<>();
		Map<Allegiance, Allegiance> allegiances = new HashMap<>();
		Map<Security, Security> securities = new HashMap<>();
		Map<State, State> states = new HashMap<>();
		for(de.sntr.eddb.data.System system: crawlStarSystems()) {
			if(system.getFaction() != null) {
				factions.put(system.getFaction(), system.getFaction());
			}
			if(system.getGovernment() != null) {
				governments.put(system.getGovernment(), system.getGovernment());
			}
			if(system.getAllegiance() != null) {
				allegiances.put(system.getAllegiance(), system.getAllegiance());
			}
			if(system.getSecurity() != null) {
				securities.put(system.getSecurity(), system.getSecurity());
			}
			if(system.getState() != null) {
				states.put(system.getState(), system.getState());
			}
			result.add(gson.toJson(system));
		}
		result.add("");
		result.add("Factions:");
		for (Faction faction : factions.values()) {
			result.add(gson.toJson(faction));
		}
		result.add("");
		result.add("Allegiances:");
		for (Allegiance allegiance : allegiances.values()) {
			result.add(gson.toJson(allegiance));
		}
		result.add("");
		result.add("Governments:");
		for (Government government : governments.values()) {
			result.add(gson.toJson(government));
		}
		result.add("");
		result.add("States:");
		for (State state : states.values()) {
			result.add(gson.toJson(state));
		}
		result.add("");
		result.add("Securities:");
		for (Security securitie : securities.values()) {
			result.add(gson.toJson(securitie));
		}
		result.add("");
		return result;
		
		
	}
	
	public static void main(String[] args) {
		new EliteDangerousWikiaCrawler().crawlStarSystems();
	}

}
