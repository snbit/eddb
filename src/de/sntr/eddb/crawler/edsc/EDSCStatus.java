package de.sntr.eddb.crawler.edsc;

import de.sntr.eddb.utils.MyStringBuffer;

public class EDSCStatus {

	private int statusnum;
	private String msg;
	
	public int getStatusnum() {
		return statusnum;
	}
	public void setStatusnum(int statusnum) {
		this.statusnum = statusnum;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		MyStringBuffer sb = new MyStringBuffer();
		
		sb.append("EDSCStatus: num=").append(String.valueOf(statusnum)).append(" msg=").append(msg).appendnl();
		
		return sb.toString();
	}
	
}
