package de.sntr.eddb.crawler.edsc;

import de.sntr.eddb.utils.MyStringBuffer;

public class EDSCGetSystemsResponse {

	private float ver;
	private String date;
	private EDSCInput status;
	private EDSCSystem[] systems;
	
	public float getVer() {
		return ver;
	}
	public void setVer(float ver) {
		this.ver = ver;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public EDSCInput getStatus() {
		return status;
	}
	public void setStatus(EDSCInput status) {
		this.status = status;
	}
	public EDSCSystem[] getSystems() {
		return systems;
	}
	public void setSystems(EDSCSystem[] systems) {
		this.systems = systems;
	}
	
	@Override
	public String toString() {
		MyStringBuffer sb = new MyStringBuffer();
		
		sb.append("EDSC GetSystems Response").appendnl();
		sb.append("Header: date=").append(date).append(" ver=").append(String.valueOf(ver)).appendnl();
		sb.append(status.toString()).appendnl();
		
		for (int i = 0; i < systems.length; i++) {
			sb.append(systems[i].toString()).appendnl();
		}
		
		return sb.toString();
	}
	
}
