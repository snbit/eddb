package de.sntr.eddb.crawler.edsc;

public class EDSCGetSystemsRequest {

	private int ver = 2; // >=1
	private boolean test = true; //optional, default=false
	private int outputmode = 2; //optional, default=1 (short/long)
	private EDSCFilter filter = null; //optional
	
	public int getVer() {
		return ver;
	}
	public void setVer(int ver) {
		this.ver = ver;
	}
	public boolean isTest() {
		return test;
	}
	public void setTest(boolean test) {
		this.test = test;
	}
	public int getOutputmode() {
		return outputmode;
	}
	public void setOutputmode(int outputmode) {
		this.outputmode = outputmode;
	}
	public EDSCFilter getFilter() {
		return filter;
	}
	public void setFilter(EDSCFilter filter) {
		this.filter = filter;
	}
	
}
