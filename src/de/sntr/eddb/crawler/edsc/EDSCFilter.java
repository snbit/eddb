package de.sntr.eddb.crawler.edsc;

public class EDSCFilter {
	
	private int knownstatus = 1; //optional
//	knownstatus filters on if a systems coordinates are known or not
//	0 (All) Default - Returns all systems, ie. "no filter".
//	1 (Known) - Returns only systems with known coordinates.
//	2 (Unknown) - Returns only systems not having known coordinates.
	private String systemname = null; //optional
	private int cr = 5; //>=0, optional, default=5
	private String date = null; //"yyyy-MM-dd hh:mm:ss" || "yyyy-MM-dd", optional, default=Now-24h
	private float[][] coordcube = null; // [[xmin,xmax][ymin,ymax][zmin,zmax]], Optional, min is inclusive ">=", max is exlusive "<"
	private EDSCCoordSphere coordsphere = null; //optional, radius is inclusive ">=
	
	public int getKnownstatus() {
		return knownstatus;
	}
	public void setKnownstatus(int knownstatus) {
		this.knownstatus = knownstatus;
	}
	public String getSystemname() {
		return systemname;
	}
	public void setSystemname(String systemname) {
		this.systemname = systemname;
	}
	public int getCr() {
		return cr;
	}
	public void setCr(int cr) {
		this.cr = cr;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public float[][] getCoordcube() {
		return coordcube;
	}
	public void setCoordcube(float[][] coordcube) {
		this.coordcube = coordcube;
	}
	public EDSCCoordSphere getCoordsphere() {
		return coordsphere;
	}
	public void setCoordsphere(EDSCCoordSphere coordsphere) {
		this.coordsphere = coordsphere;
	}
	
	
}
