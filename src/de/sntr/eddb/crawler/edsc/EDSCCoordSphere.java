package de.sntr.eddb.crawler.edsc;

public class EDSCCoordSphere {
	
	private float radius = 0;
	private float[] origin = null;
	
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public float[] getOrigin() {
		return origin;
	}
	public void setOrigin(float[] origin) {
		this.origin = origin;
	}
	
}
