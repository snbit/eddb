package de.sntr.eddb.crawler.edsc;

import de.sntr.eddb.utils.MyStringBuffer;

public class EDSCInput {

	private EDSCStatus[] status;

	public EDSCStatus[] getStatus() {
		return status;
	}

	public void setStatus(EDSCStatus[] status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		MyStringBuffer sb = new MyStringBuffer();
		
		if (status != null) {
			for (int i = 0; i < status.length; i++) {
				sb.append(status[i].toString());
			}
		}
		return sb.toString();
	}
	
}
