package de.sntr.eddb.crawler.edsc;

import org.apache.commons.lang3.StringUtils;

import de.sntr.eddb.utils.MyStringBuffer;

public class EDSCSystem {

	private int id;
	private String name;
	private float[] coord;
	private int cr;
	private String commandercreate;
	private String createdate;
	private String commanderupdate;
	private String updatedate;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float[] getCoord() {
		return coord;
	}
	public void setCoord(float[] coord) {
		this.coord = coord;
	}
	public int getCr() {
		return cr;
	}
	public void setCr(int cr) {
		this.cr = cr;
	}
	public String getCommandercreate() {
		return commandercreate;
	}
	public void setCommandercreate(String commandercreate) {
		this.commandercreate = commandercreate;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getCommanderupdate() {
		return commanderupdate;
	}
	public void setCommanderupdate(String commanderupdate) {
		this.commanderupdate = commanderupdate;
	}
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	
	@Override
	public String toString() {
		MyStringBuffer sb = new MyStringBuffer();
		
		sb.append("Id: ").append(String.valueOf(id)).appendnl();
		sb.append("System: ").append(name).appendnl();
		sb.append("Coords: {");
		for(int i = 0; i<3; i++) {
			sb.append(String.valueOf(coord[i]));
			if(i<2) sb.append(", ");
			else sb.append("}").appendnl();
		}
		sb.append("Create: ").append(commandercreate).append(" - ").append(createdate).appendnl();
		sb.append("Update: ").append(commanderupdate).append(" - ").append(updatedate).appendnl();		
		
		return sb.toString();
	}
}
