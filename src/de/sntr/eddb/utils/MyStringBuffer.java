package de.sntr.eddb.utils;

public class MyStringBuffer {
	
	StringBuffer sb;
	
	public MyStringBuffer() {
		sb = new StringBuffer();
	}
	
	public MyStringBuffer append(String s) {
		sb.append(s);
		return this;
	}
	
	public MyStringBuffer appendnl() {
		sb.append('\n');
		return this;
	}

	@Override
	public String toString() {
		return sb.toString();
	}
}
