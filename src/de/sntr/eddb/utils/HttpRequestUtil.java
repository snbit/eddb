package de.sntr.eddb.utils;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


public class HttpRequestUtil {

	private static CloseableHttpClient httpClient = HttpClients.createDefault();
	
	protected HttpRequestUtil() {
		httpClient = HttpClients.createDefault();
	}
	
	public static String request (String address) {
		HttpGet httpGet = new HttpGet(address);
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(httpGet);
			return IOUtils.toString(response.getEntity().getContent());
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		    try {
				response.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
