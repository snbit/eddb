/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.sntr.eddb.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate_eddb.cfg.xml");
            StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder();
            ServiceRegistry serviceRegistry = ssrb.applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session beginTransaction() {
        Session hibernateSession = HibernateUtil.getSession();
        hibernateSession.beginTransaction();
        return hibernateSession;
    }

    public static void commitTransaction() {
        HibernateUtil.getSession().getTransaction().commit();
    }

    public static void rollbackTransaction() {
        HibernateUtil.getSession().getTransaction().rollback();
    }

    public static void closeSession() {
        HibernateUtil.getSession().close();
    }

    public static Session getSession() {
        Session hibernateSession = sessionFactory.getCurrentSession();
        return hibernateSession;
    }
    
    public static void sessionFlushClear() {
		getSession().flush();
		getSession().clear();
    }
    
//    public static void closeSessionFactory() {
//    	SessionFactory factory = sessionFactory;
//    	if (factory instanceof SessionFactoryImpl) {
//    		SessionFactoryImpl sf = (SessionFactoryImpl) factory;
//    		ConnectionProvider conn = sf.getConnectionProvider();
//    		if (conn instanceof C3P0ConnectionProvider) {
//    			((C3P0ConnectionProvider) conn).close();
//    		}
//    	}
//    	factory.close();
//    }
}
