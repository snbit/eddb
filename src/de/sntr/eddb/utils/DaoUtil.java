package de.sntr.eddb.utils;

import org.hibernate.Query;
import org.hibernate.Session;

public class DaoUtil {

    
    public static Object findByName(String name, Class clazz) {
    	Session session = HibernateUtil.getSession();
    	
    	Query query = session.createQuery("from " +clazz.getName() +" where name= :sName");
    	query.setParameter("sName", name);
//    	query.setParameter(":clazz", clazz.getName());
    	return query.uniqueResult();
    }
}
