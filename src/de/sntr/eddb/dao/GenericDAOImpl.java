/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.sntr.eddb.dao;

import de.sntr.eddb.utils.HibernateUtil;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
 

public abstract class GenericDAOImpl<T, ID extends Serializable> implements GenericDAO<T, ID> {
 
    protected Session getSession() {
        return HibernateUtil.getSession();
    }
 
    @Override
    public void persist(T entity) {
    	Session hibernateSession = this.getSession();
    	hibernateSession.persist(entity);
    }
    
    public void save(T entity) {
        Session hibernateSession = this.getSession();
        hibernateSession.saveOrUpdate(entity);
    }
 
    public void merge(T entity) {
        Session hibernateSession = this.getSession();
        hibernateSession.merge(entity);
    }
 
    public void delete(T entity) {
        Session hibernateSession = this.getSession();
        hibernateSession.delete(entity);
    }
 
    public List<T> findMany(Query query) {
        List<T> t;
        t = (List<T>) query.list();
        return t;
    }
 
    public T findOne(Query query) {
        T t;
        t = (T) query.uniqueResult();
//        System.out.println(t);
        return t;
    }
 
    public T findByID(Class clazz, Integer id) {
        Session hibernateSession = this.getSession();
        T t = null;
        t = (T) hibernateSession.get(clazz, id);
        return t;
    }
 
    public List findAll(Class clazz) {
        Session hibernateSession = this.getSession();
        List T = null;
        Query query = hibernateSession.createQuery("from " + clazz.getName());
        T = query.list();
        return T;
    }
    
    public T findByName(String name, Class clazz) {
    	Session session = this.getSession();
//    	if(entityName.contains("'")) {
//			entityName.replace("'", "''");
//		}
//    	Query query = session.createQuery("from " +clazz.getName() +" where name=\"" +name +"\"");
    	Query query = session.createQuery("from " +clazz.getName() +" where name = :sName");
    	query.setParameter("sName", name);
//    	System.out.println(query.getQueryString());
//    	query.setParameter(":clazz", clazz.getName());
    	return this.findOne(query);
    }
    
    public List<T> findByNameFragment(String fragment, Class clazz) {
    	Session session = this.getSession();
    	Query query = session.createQuery("from " +clazz.getName() +" where name like '%"+fragment+"%' order by name");
    	//query.setParameter("sFragment", "'%"+fragment+"%'");
    	return this.findMany(query);
    }
}
