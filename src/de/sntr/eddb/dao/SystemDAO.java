package de.sntr.eddb.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.hibernate.Query;
import org.hibernate.Session;

import de.sntr.eddb.data.Allegiance;
import de.sntr.eddb.data.Faction;
import de.sntr.eddb.data.Government;
import de.sntr.eddb.data.Security;
import de.sntr.eddb.data.State;
import de.sntr.eddb.utils.DaoUtil;
import de.sntr.eddb.utils.HibernateUtil;

public class SystemDAO extends GenericDAOImpl<de.sntr.eddb.data.System, Integer>{

	private final Logger log = Logger.getLogger(this.getClass().getName());
	
    @Override
    public void merge(de.sntr.eddb.data.System entity) {
    	Session session = this.getSession();
    	log.fine("Start merging System: " +entity.getName());
    	String entityName = entity.getName();
		
    	de.sntr.eddb.data.System system = this.findByName(entityName, de.sntr.eddb.data.System.class);
//    	System.out.println("Merging System: " +entity.getName());
    	if(system == null) {
//    		System.out.println("System is new");
    		system = entity;
    	} else {
//    		System.out.println("System exists");
			String name = entity.getName();
//			if(name.contains("'")) {
//				name.replace("'", "\'");
//			}
    		system.setName(name);
    		system.setX(entity.getX());
    		system.setY(entity.getY());
    		system.setZ(entity.getZ());
    	}
		if(entity.getAllegiance() != null) {
			Allegiance all = (Allegiance) DaoUtil.findByName(entity.getAllegiance().getName(), Allegiance.class);
			if(all != null) {
				system.setAllegiance(all);
			} else {
				system.setAllegiance(entity.getAllegiance());
			}
		}
		if(entity.getFaction() != null) {
			Faction all = (Faction) DaoUtil.findByName(entity.getFaction().getName(), Faction.class);
			if(all != null) {
				system.setFaction(all);
			} else {
				system.setFaction(entity.getFaction());
			}
		}
		if(entity.getGovernment() != null) {
			Government all = (Government) DaoUtil.findByName(entity.getGovernment().getName(), Government.class);
			if(all != null) {
				system.setGovernment(all);
			} else {
				system.setGovernment(entity.getGovernment());
			}
		}
		if(entity.getSecurity() != null) {
			Security all = (Security) DaoUtil.findByName(entity.getSecurity().getName(), Security.class);
			if(all != null) {
				system.setSecurity(all);
			} else {
				system.setSecurity(entity.getSecurity());
			}
		}
		if(entity.getState() != null) {
			State all = (State) DaoUtil.findByName(entity.getState().getName(), State.class);
			if(all != null) {
				system.setState(all);
			} else {
				system.setState(entity.getState());
			}
		}
//		System.out.println("Merging now: " +system.getName());
//		System.out.println();
    	session.merge(system);
    }
    
    public List<de.sntr.eddb.data.System> findNeighbours(String name, int radius) {
//    	Set<de.sntr.eddb.data.System> systems = new HashSet<de.sntr.eddb.data.System>();
    	
    	de.sntr.eddb.data.System system = this.findByName(name, de.sntr.eddb.data.System.class);
    	System.out.println(system.getName());
    	if(system == null) {
    		return null;
    	}
    	
    	Query query = this.getSession().createQuery(" from System where power(x-:x, 2)+power(y-:y, 2)+power(z-:z, 2)<=power(32*:r, 2) and (x != 0 and y != 0 and z != 0) and name != :name");
    	query.setParameter("x", system.getX());
    	query.setParameter("y", system.getY());
    	query.setParameter("z", system.getZ());
    	query.setParameter("r", radius);
    	query.setParameter("name", name);
    	return query.list();
    	
//    	return systems;
    }
    
//    public de.sntr.eddb.data.System findByName(String name) {
//    	Session session = this.getSession();
//    	
//    	Query query = session.getNamedQuery("from System s where s.name=:sName");
//    	query.setParameter("sName", name);
//    	return this.findOne(query);
//    }
	
}
