package de.sntr.eddb.scratch;

import de.sntr.eddb.data.*;
import de.sntr.eddb.data.System;

public class SystemLayoutTest {

	public static void main(String[] args) {
		de.sntr.eddb.data.System system = new System("Eranin");
		
		Faction faction = new Faction("Eranin Peoples Party");
		Government government = new Government("Communism");
		State state = new State("Boom");
		Allegiance allegiance = new Allegiance("Independent");
		Security security = new Security("Medium");
		
		system.setFaction(faction);
		system.setGovernment(government);
		system.setState(state);
		system.setAllegiance(allegiance);
		system.setSecurity(security);
		system.setPopulation(450000);
		
		Star eranin = new Star("Eranin");
		eranin.setParentSystem(system);
		system.setPrimaryStar(eranin);
		
		Planet azeban = new Planet("Azeban");
		azeban.setParent(eranin);
		
		Station orbital = new Station("Azeban Orbital");
		orbital.setParent(azeban);
		
		Station city = new Station("Azeban City");
		city.setParent(azeban);
		
		Planet planet = new Planet("Eranin 2");
		planet.setParent(eranin);
		planet = new Planet("Eranin 4");
		
		Station survey = new Station("Eranin 4 Survey");
		survey.setParent(planet);
		
		
		
	}

}
