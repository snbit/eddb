package de.sntr.eddb.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "AsteroidBelts")
//@Table(name = "AsteroidBelts")
public class AsteroidBelt extends AstroObject {

	private static final long serialVersionUID = 1L;

	private AsteroidBeltType asteroidBeltType;

	@ManyToOne
	@JoinColumn(name = "ref_type")
	public AsteroidBeltType getAsteroidBeltType() {
		return asteroidBeltType;
	}

	public void setAsteroidBeltType(AsteroidBeltType asteroidBeltType) {
		this.asteroidBeltType = asteroidBeltType;
	}
}
