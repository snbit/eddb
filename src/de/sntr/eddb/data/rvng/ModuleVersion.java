package de.sntr.eddb.data.rvng;

// Generated 11.01.2015 09:26:44 by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ModuleVersion generated by hbm2java
 */
@Entity
@Table(name = "ModuleVersions", catalog = "eddb")
public class ModuleVersion implements java.io.Serializable {

	private Integer id;
	private String name;

	public ModuleVersion() {
	}

	public ModuleVersion(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
