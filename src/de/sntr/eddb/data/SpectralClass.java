package de.sntr.eddb.data;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

// Generated 11.01.2015 07:52:00 by Hibernate Tools 3.4.0.CR1

/**
 * SpectralClass generated by hbm2java
 */
@Entity
@Table(name = "SpectralClasses")
public class SpectralClass implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String spectralClass;
	private String conventionalColor;

	public SpectralClass() {
	}

	public SpectralClass(String spectralClass, String conventionalColor) {
		this.spectralClass = spectralClass;
		this.conventionalColor = conventionalColor;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 5)
	public String getSpectralClass() {
		return this.spectralClass;
	}

	public void setSpectralClass(String spectralClass) {
		this.spectralClass = spectralClass;
	}

	@Column(name = "conventionalColor", nullable = false, length = 20)
	public String getConventionalColor() {
		return this.conventionalColor;
	}

	public void setConventionalColor(String conventionalColor) {
		this.conventionalColor = conventionalColor;
	}

}
