package de.sntr.eddb.data;

// Generated 11.01.2015 07:52:00 by Hibernate Tools 3.4.0.CR1

/**
 * Commodity generated by hbm2java
 */
public class Commodity implements java.io.Serializable {

	private Integer id;
	private String name;
	private int refCategory;
	private Integer galaxyAverage;

	public Commodity() {
	}

	public Commodity(String name, int refCategory) {
		this.name = name;
		this.refCategory = refCategory;
	}

	public Commodity(String name, int refCategory, Integer galaxyAverage) {
		this.name = name;
		this.refCategory = refCategory;
		this.galaxyAverage = galaxyAverage;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRefCategory() {
		return this.refCategory;
	}

	public void setRefCategory(int refCategory) {
		this.refCategory = refCategory;
	}

	public Integer getGalaxyAverage() {
		return this.galaxyAverage;
	}

	public void setGalaxyAverage(Integer galaxyAverage) {
		this.galaxyAverage = galaxyAverage;
	}

}
