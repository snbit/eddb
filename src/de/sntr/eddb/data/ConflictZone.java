package de.sntr.eddb.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "ConflictZones")
//@Table(name = "ConflictZones")
public class ConflictZone extends AstroObject {

	private static final long serialVersionUID = 1L;
	
	private ConflictZoneIntesity intensity;

	@ManyToOne
	@JoinColumn(name = "ref_intensity")
	public ConflictZoneIntesity getIntensity() {
		return intensity;
	}

	public void setIntensity(ConflictZoneIntesity intensity) {
		this.intensity = intensity;
	}
}
