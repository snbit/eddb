package de.sntr.eddb.data;

// Generated 11.01.2015 07:52:00 by Hibernate Tools 3.4.0.CR1

import java.util.Date;

/**
 * Price generated by hbm2java
 */
public class Price implements java.io.Serializable {

	private Integer id;
	private int refStation;
	private int refCommodity;
	private Integer buy;
	private Integer sell;
	private Integer demand;
	private Integer supply;
	private Date date;

	public Price() {
	}

	public Price(int refStation, int refCommodity, Date date) {
		this.refStation = refStation;
		this.refCommodity = refCommodity;
		this.date = date;
	}

	public Price(int refStation, int refCommodity, Integer buy, Integer sell,
			Integer demand, Integer supply, Date date) {
		this.refStation = refStation;
		this.refCommodity = refCommodity;
		this.buy = buy;
		this.sell = sell;
		this.demand = demand;
		this.supply = supply;
		this.date = date;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getRefStation() {
		return this.refStation;
	}

	public void setRefStation(int refStation) {
		this.refStation = refStation;
	}

	public int getRefCommodity() {
		return this.refCommodity;
	}

	public void setRefCommodity(int refCommodity) {
		this.refCommodity = refCommodity;
	}

	public Integer getBuy() {
		return this.buy;
	}

	public void setBuy(Integer buy) {
		this.buy = buy;
	}

	public Integer getSell() {
		return this.sell;
	}

	public void setSell(Integer sell) {
		this.sell = sell;
	}

	public Integer getDemand() {
		return this.demand;
	}

	public void setDemand(Integer demand) {
		this.demand = demand;
	}

	public Integer getSupply() {
		return this.supply;
	}

	public void setSupply(Integer supply) {
		this.supply = supply;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
