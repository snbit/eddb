package de.sntr.eddb.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.Document.OutputSettings;

import de.sntr.eddb.crawler.EliteDangerousWikiaCrawler;
import de.sntr.eddb.dao.SystemDAO;
import de.sntr.eddb.utils.HibernateUtil;

/**
 * Servlet implementation class CrawlWikiaServlet
 */
public class CrawlWikiaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final Logger log = Logger.getLogger(this.getClass().getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CrawlWikiaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("Wikia Crawler Servlet");
		Set<de.sntr.eddb.data.System> systems = new EliteDangerousWikiaCrawler().crawlStarSystems();
		SystemDAO systemDao = new SystemDAO();
        HibernateUtil.beginTransaction();
        Iterator<de.sntr.eddb.data.System> iter = systems.iterator();
        Document doc = Jsoup.parse("");
        Element e = doc.body();
        e.appendElement("p").appendText("Found " +systems.size() +"systems on wikia. Updating now...");
        PrintWriter out = response.getWriter();
        Element p = e.appendElement("p");
        while(iter.hasNext()) {
        	de.sntr.eddb.data.System system = iter.next();
        	p.text(p.text() +system.getName());
        	if(iter.hasNext()) {
        		p.text(p.text() +",&nbsp");
        	} 
        	systemDao.merge(system);
        }
        e.appendElement("p").text("Finished Updating");
        out.println(doc.toString());
        HibernateUtil.commitTransaction();
        HibernateUtil.closeSession();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
