package de.sntr.eddb.servlet;

import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebAppListener implements ServletContextListener {

	Logger log = Logger.getLogger(this.getClass().getName());
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		//credit: http://techblog.ralph-schuster.eu/2014/07/09/solution-to-tomcat-cant-stop-an-abandoned-connection-cleanup-thread/
		try {
			log.warning("Manually shutting down " +com.mysql.jdbc.AbandonedConnectionCleanupThread.class.getName() +" to prevent memory leaks.");
			com.mysql.jdbc.AbandonedConnectionCleanupThread.shutdown();
		} catch (Throwable t) {
		}
		// This manually deregisters JDBC driver, which prevents Tomcat 7 from
		// complaining about memory leaks
		Enumeration<java.sql.Driver> drivers = java.sql.DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			java.sql.Driver driver = drivers.nextElement();
			try {
				java.sql.DriverManager.deregisterDriver(driver);
			} catch (Throwable t) {
			}
		}
		try {
			Thread.sleep(2000L);
		} catch (Exception e) {
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

}
