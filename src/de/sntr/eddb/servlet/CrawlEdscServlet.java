package de.sntr.eddb.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.google.gson.Gson;

import de.sntr.eddb.crawler.EDSCCrawler;
import de.sntr.eddb.dao.SystemDAO;
import de.sntr.eddb.utils.HibernateUtil;

/**
 * Servlet implementation class CrawlEdscServlet
 */
public class CrawlEdscServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	Logger log = Logger.getLogger(this.getClass().getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CrawlEdscServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SystemDAO systemDao = new SystemDAO();

    	HibernateUtil.beginTransaction();
        EDSCCrawler crawler = new EDSCCrawler();
        Date oldestDate = EDSCCrawler.oldestDate;
        Date testDate = null;
        Document doc = Jsoup.parse("");
        Element e = doc.body();
        PrintWriter out = response.getWriter();
		try {
			testDate = EDSCCrawler.edscFormat.parse("2014-11-26");
			log.finest("Starting Crawler");
	        Set<de.sntr.eddb.data.System> systems = crawler.crawlStarSystems(oldestDate);
	        log.finest("Crawling finished");
	        e.appendElement("p").appendText("Got " +systems.size() +" systems from EDSC. Updating now...");
	        Iterator<de.sntr.eddb.data.System> iter = systems.iterator();
	        log.finest("Updating database");
	        int count = 0;
	        while(iter.hasNext()) {
	        	de.sntr.eddb.data.System system = iter.next();
	        	log.finest("Merging system: " +system.getName());
	        	systemDao.merge(system);
	        	if(count % 15 == 0) {
	        		HibernateUtil.sessionFlushClear();
	        	}
	        }
	        
	        e.appendElement("p").appendText("Finished Update");
	        out.println(doc.toString());

        	HibernateUtil.commitTransaction();
        	HibernateUtil.closeSession();
	        
		} catch (Exception ex) {
			HibernateUtil.rollbackTransaction();
			ex.printStackTrace();
		} finally {
			HibernateUtil.closeSession();
		}
        
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
