package de.sntr.eddb.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import de.sntr.eddb.dao.SystemDAO;
import de.sntr.eddb.utils.HibernateUtil;

/**
 * Servlet implementation class GetSystemServlet
 */
public class GetPostSystemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPostSystemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String search = request.getParameter("search");
		if(name != null) {
			HibernateUtil.beginTransaction();
			de.sntr.eddb.data.System system = new SystemDAO().findByName(name, de.sntr.eddb.data.System.class);
			HibernateUtil.commitTransaction();
//			System.out.println(new Gson().toJson(system));
			if(system != null) {
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().print(new Gson().toJson(system));
			}
		} else if (search != null) {
			HibernateUtil.beginTransaction();
			List<de.sntr.eddb.data.System> systems = new SystemDAO().findByNameFragment(search, de.sntr.eddb.data.System.class);
			HibernateUtil.commitTransaction();
//			System.out.println(new Gson().toJson(system));
			if(systems != null && !systems.isEmpty()) {
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().print(new Gson().toJson(systems));
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
