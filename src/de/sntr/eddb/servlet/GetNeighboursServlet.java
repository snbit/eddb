package de.sntr.eddb.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import de.sntr.eddb.dao.SystemDAO;
import de.sntr.eddb.utils.HibernateUtil;

/**
 * Servlet implementation class GetNeighboursServlet
 */
@WebServlet("/GetNeighboursServlet")
public class GetNeighboursServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetNeighboursServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String radiusParam = request.getParameter("radius");
		if(name == null || radiusParam == null){
			return;
		}
		int radius = Integer.parseInt(radiusParam);
		if(name != null && radius != 0) {
			HibernateUtil.beginTransaction();
			List<de.sntr.eddb.data.System> systems = new SystemDAO().findNeighbours(name, radius);
			HibernateUtil.commitTransaction();
//			System.out.println(new Gson().toJson(systems));
			if(systems.size() > 0) {
				response.setContentType("application/json");
				response.addHeader("Access-Control-Allow-Origin", "*");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().print(new Gson().toJson(systems));
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
