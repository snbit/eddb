package de.sntr.eddb.scratch;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import jdk.nashorn.internal.runtime.JSONFunctions;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

/**
 * Servlet implementation class Service
 */
@WebServlet("/Service")
public class Service extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Service() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html>");
		out.println("<head>");
		out.println("<title>Test</title>");
		out.println("</head>");
		out.println("<body bgcolor=\"white\">");
		//out.println(request.getParameterMap().toString());
		
		int version = Integer.parseInt(request.getParameter("version"));
		boolean test = Boolean.parseBoolean(request.getParameter("test"));
		int outputMode = Integer.parseInt(request.getParameter("outputmode"));
		int knownStatus = Integer.parseInt(request.getParameter("status"));
		String systemName = request.getParameter("name");
		int confRating = Integer.parseInt(request.getParameter("rating"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = null;
		try {
			date = sdf.parse(request.getParameter("date"));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		float radius = Float.parseFloat(request.getParameter("radius").replace(',', '.'));
		int originX = Integer.parseInt(request.getParameter("originx"));
		int originY = Integer.parseInt(request.getParameter("originy"));
		int originZ = Integer.parseInt(request.getParameter("originz"));
		
		

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpPost post = new HttpPost("http://edstarcoordinator.com/api.asmx/GetSystems");
            
            StringEntity params = new StringEntity("{data:{ver:2, test:true, outputmode:2, filter:{knownstatus:0, //systemname: 'sol', cr:5, date:'2014-09-18 12:34:56', //coordcube: [[-10,10],[-10,10],[-10,10]], coordsphere: {radius: 12.45, origin: [0,0,0]}}}");
            JSONObject jo = new JSONObject();
            JSONObject filter = new JSONObject();
            filter.put("knownstatus", knownStatus);
            filter.put("date", sdf.format(date));
            JSONObject coordsphere = new JSONObject();
            coordsphere.put("radius", radius);
            //coordsphere.put("origin", "[0,0,0]");
            JSONArray jArray = new JSONArray();
            jArray.add(originX);jArray.add(originY);jArray.add(originZ);
            coordsphere.put("origin", jArray);
            filter.put("coordsphere", coordsphere);
            jo.put("ver", version);
            jo.put("test", test);
            jo.put("outputmode", outputMode);
            jo.put("filter", filter);
            JSONObject jo2 = new JSONObject();
            jo2.put("data", jo);
            //out.println(jo2.toJSONString());
            params = new StringEntity(jo2.toJSONString());
            params.setContentType("application/json");
            params.setContentEncoding("UTF-8");
            
            //out.println(params.toString()+"<br/>");
            post.addHeader("content-type", "application/json; charset=utf-8");
            post.setEntity(params);
            
            HttpResponse result = httpClient.execute(post);

            String json = EntityUtils.toString(result.getEntity(), "UTF-8");
            //out.println(json + "<br/>");
            try {
                JSONParser parser = new JSONParser();
                
                Object resultObject = parser.parse(json);

                if (resultObject instanceof JSONArray) {
                    JSONArray array=(JSONArray)resultObject;
                    for (Object object : array) {
                        JSONObject obj =(JSONObject)object;
                        out.println("JSONArray:");
                        out.println(obj.toJSONString());
                        //System.out.println(obj.get("example"));
                        //System.out.println(obj.get("fr"));
                    }

                }else if (resultObject instanceof JSONObject) {
                    JSONObject obj =(JSONObject)resultObject;
                    out.println("JSONObject:");
                    out.println(obj.toJSONString());
                    JSONObject r = (JSONObject)obj.get("d");
                    JSONArray systems = (JSONArray)r.get("systems");
                    for(int i = 0; i < systems.size(); i++){
                    	JSONObject system = (JSONObject)systems.get(i);
                    	out.println((String)system.get("name") +"<br/>");
                    }
                    out.println("Class: " +r.getClass());
                }

            } catch (Exception e) {
                e.printStackTrace();;
            }

        } catch (IOException ex) {
        	ex.printStackTrace();
        }



		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
