package de.sntr.eddb.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.sntr.eddb.crawler.EDSCCrawler;

/**
 * Servlet implementation class Service
 */
@WebServlet("/Service")
public class Service extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Service() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean badRequest = false;
		String type = request.getParameter("type");
		if (type == null) {
			badRequest = true;
		}
		else {
			switch (type) {
			case "systemNeighbours":
				String system = request.getParameter("system");
				if (system == null) {
					badRequest = true;
					break;
				}
				String range = request.getParameter("range");
				try {
					Float fRange = Float.parseFloat(range);
				} catch (NumberFormatException e) {
					badRequest = true;
					//TODO log NFE
					break;
				}
				break;
			case "crawlEDSC":
				boolean update = Boolean.parseBoolean(request.getParameter("update"));
				String date = request.getParameter("date");
				if(update == false) {
					
				}
				break;
			default:
				badRequest = true;
				break;
			}
		}
		if (badRequest) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
