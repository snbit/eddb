package de.sntr.eddb;
import org.hibernate.cfg.reveng.DelegatingReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.ReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.TableIdentifier;

public class NamingReverseEngineeringStrategy extends DelegatingReverseEngineeringStrategy {

    public NamingReverseEngineeringStrategy(ReverseEngineeringStrategy delegate) {
        super(delegate);
    }

    
    public String tableToClassName(TableIdentifier tableIdentifier) {
          String className = super.tableToClassName(tableIdentifier);
          String tableName = tableIdentifier.getName();
          if(tableName.equalsIgnoreCase("Commodities")) {
        	  className = className.substring(0, className.length() -"Commodities".length()) +"Commodity";
          } else if(tableName.equalsIgnoreCase("CommodityCategories")) {
        	  className = className.substring(0, className.length() -"CommodityCategories".length()) +"CommodityCategory";
          } else if(tableName.equalsIgnoreCase("LuminosityClasses")) {
        	  className = className.substring(0, className.length() -"LuminosityClasses".length()) +"LuminosityClass";
          } else if(tableName.equalsIgnoreCase("ModuleCategories")) {
        	  className = className.substring(0, className.length() -"ModuleCategories".length()) +"ModuleCategory";
          } else if(tableName.equalsIgnoreCase("SpectralClasses")) {
        	  className = className.substring(0, className.length() -"SpectralClasses".length()) +"SpectralClass";
          } else if(tableName.equalsIgnoreCase("Economies")) {
        	  className = className.substring(0, className.length() -"Economies".length()) +"Economy";
          } else if(tableName.equalsIgnoreCase("AOTypes")) {
        	  className = className.substring(0, className.length() -"AOTypes".length()) +"AOType";
          } else if(tableName.equalsIgnoreCase("ShipsInternalCompartment")) {
        	  className = className.substring(0, className.length() -"ShipsInternalCompartment".length()) +"ShipInternalCompartment";
          } else if(tableName.equalsIgnoreCase("ShipsOutfitting")) {
        	  className = className.substring(0, className.length() -"ShipsOutfitting".length()) +"ShipOutfitting";
          } else if(tableName.equalsIgnoreCase("Systems")) {
        	  className = className.substring(0, className.length() -"Systems".length()) +"System";
          } else if(tableName.equalsIgnoreCase("Securities")) {
        	  className = className.substring(0, className.length() -"Securities".length()) +"Security";
          } else {
        	  className = className.substring(0, className.length()-1);
          }
          return className;
    }
}